from req_module import *
import xlrd
from xlrd.sheet import ctype_text
import unicodedata
from threading import Thread
from Queue import Queue
import time
import phan_proxy
from bs4 import BeautifulSoup
import re
import csv

num_fetch_threads2 = 5
enclosure_queue2 = Queue()


def str_strip(x):
        try:
            return str(x).strip()

        except:
            return x.encode('ascii', "ignore").decode('utf-8').strip()


def get_page_parse(row_n_cols, f1, f2):
    """
    :param row_n_cols:
    : link ['huffingtonpost.com', 'News & Media', '214.0', '175500000.0', '0.1875']
    :return:
    """
    url = 'http://%s/' %(row_n_cols[0])

    # req_main(url)
    driver = phan_proxy.main(url)
    page = driver.page_source
    if page:
        soup = BeautifulSoup(page)
        soup.find("span", text=re.compile("by Taboola"))

        #target_str = soup.find("span", text=re.compile('by Taboola')).parent.parent.parent.parent
        try:
            target_str = soup.find("div", attrs={"class":"trc_rbox"}).get("id")
            row_n_cols.insert(1, target_str)
            info2 = map(str_strip,row_n_cols)
            f1.write(", ".join(info2)+"\n")
            logging.debug(target_str)
            logging.debug("%s ==== scraped" % url)

        except:
            target_str = ''
            row_n_cols.insert(1, target_str)
            info2 = map(str_strip,row_n_cols)
            f2.write(", ".join(info2)+"\n")
            logging.debug("%s not scraped" % url)

       # import pdb;pdb.set_trace()

        
    driver.delete_all_cookies()
    driver.quit()



def process_enqueu(i, q):
    for row_n_cols, f1, f2  in iter(q.get, None):
        try:
            get_page_parse(row_n_cols, f1, f2)
            logging.debug(row_n_cols)
        except:
             pass

        time.sleep(2)
        q.task_done()

    q.task_done()


def parse_taboola_xlxs():
    f_name = os.path.join(DIR_REQ, 'SimilarTechReportTaboola.xlsx')

    xl_workbook = xlrd.open_workbook(f_name)
    xl_sheet = xl_workbook.sheet_by_index(0)

    row = xl_sheet.row(1)

    map_first_row = dict()

    procs = []

    f1 = open('success.csv', 'a+')
    f1.write(", ".join(['Link', 'Id', 'Category', 'Rank','Visit','Tech Rank']) + "\n")

    f2 = open('not_success.csv', 'a+')
    f2.write(", ".join(['Link', 'Id', 'Category', 'Rank','Visit','Tech Rank']) + "\n")

    for i in range(num_fetch_threads2):
        procs.append(Thread(target=process_enqueu, args=(i, enclosure_queue2,)))
        procs[-1].start()


    for idx, cell_obj in enumerate(row):
        cell_type_str = ctype_text.get(cell_obj.ctype, 'unknown type')
        print('(%s) %s %s' % (idx, cell_type_str, cell_obj.value))
        map_first_row[cell_obj.value] = idx

    num_cols = xl_sheet.ncols

    for row_idx in range(0, xl_sheet.nrows):
        row_n_cols = map(str_strip, xl_sheet.row_values(row_idx))
        enclosure_queue2.put((row_n_cols,f1,f2))

    enclosure_queue2.join()

    for p in procs:
        enclosure_queue2.put(None)

    enclosure_queue2.join()

    for p in procs:
        p.join(180)


if __name__=="__main__":
    parse_taboola_xlxs()
    #row_n_cols = ['http://www.iflscience.com/', 'News & Media', '214.0', '175500000.0', '0.1875']
    # get_page_parse(row_n_cols)
