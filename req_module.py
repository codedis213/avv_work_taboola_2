from requests import Session
import os
from random import choice
import logging

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DIR_REQ = os.path.join(BASE_DIR, 'avv_work_taboola_2')
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s', )



def proxy_to_list():
    """
    :return:list of proxies
    """
    req_dir = DIR_REQ

    f_name = "%s/%s" % (req_dir, "proxies.txt")

    with open(f_name) as f:
        list_of_proxies = f.readlines()

    return list_of_proxies


def proxy_spilt(list_of_proxies):
    """
    :
    :param list_of_proxies:
    :like  104.144.27.72:80:ajay79:india123
    :return:one proxies_url
    """
    proxy_str = choice(list_of_proxies)

    proxy_str_lst = proxy_str.strip().split(":")
    # proxies_url = "http://eric316:india123@91.108.180.243:80/"
    proxies_url = "http://%s:%s@%s:%s/" %(proxy_str_lst[2], proxy_str_lst[3], proxy_str_lst[0], proxy_str_lst[1])

    return proxies_url


def proxy_to_list_8080():
    """
    :return:list of proxies
    """
    req_dir = DIR_REQ

    f_name = "%s/%s" % (req_dir, "proxy_8080.txt")

    with open(f_name) as f:
        list_of_proxies = f.readlines()

    return list_of_proxies


def proxy_spilt_8080(list_of_proxies):
    """
    :
    :param list_of_proxies:
    :like  104.144.27.72:80:ajay79:india123
    :return:one proxies_url
    """
    proxy_str = choice(list_of_proxies).strip()
    proxies_url = "http://%s:8080/" % proxy_str

    return proxies_url




def req_module(list_of_proxies, url):
    headers = {'User-Agent':'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:23.0) Gecko/20100101 Firefox/23.0'}

    session = Session()

    for idx in xrange(3):
        # proxies_url = proxy_spilt_8080(list_of_proxies)
        proxies_url = proxy_spilt(list_of_proxies)
        proxydict = {"http": proxies_url, "https": proxies_url}
        try:
            logging.debug(proxies_url)
            r = session.get(url,  proxies=proxydict, headers=proxydict, timeout=10)
            page = r.content
            r.close()
            return page
        except:
            pass

    return None


def req_main(url):
    list_of_proxies = proxy_to_list()
    # list_of_proxies = proxy_to_list_8080()
    page = req_module(list_of_proxies, url)
    return page


if __name__ == "__main__":
    url = "http://www.iflscience.com/"
    print req_main(url)


